<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    public function verify(Request $request)
    {
        if ($request->header('Authorization') && $token = JWTAuth::parseToken())
        {
            return response()->json(true, 200);
        } else {
            return response()->json(false, 200);
        }
    }
	public function authenticate(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('username', 'password');
		try {
			$user = User::where('username', $request->get('username'))->first();
			if ($user && $user->blocked)
			{
				return response()->json('Аккаунт заблокирован', 401);
			}
			if (! $token = JWTAuth::attempt($credentials)) {
				return response()->json('Неверные данные', 401);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json('Ошибка сервера', 500);
		}

		// all good so return the token
		$permissions = $user->getAllPermissions()->pluck('name');
		$role = $user->getRoleNames()[0];
		return response()->json([
			'token' => $token, 
			'permissions' => $permissions, 
			'role' => $role,
			'user' => [
				'id' => $user['id'],
				'username' => $user['username']
			],
		]);
	}
}
