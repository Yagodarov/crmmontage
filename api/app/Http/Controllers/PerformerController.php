<?php

namespace App\Http\Controllers;

use App\Models\Performer as BaseModel;
use Illuminate\Http\Request;

class PerformerController extends Controller
{
    public function index(Request $request)
    {
        return BaseModel::search($request);
    }

    public function get($id)
    {
        $model = BaseModel::find($id);
        return response()->json($model);
    }

    public function store(Request $request)
    {
        return (new BaseModel())->store($request);
    }

    public function update(Request $request)
    {
        $model = BaseModel::find($request->get('id'));
        return ($model->storeUpdate($request));
    }

    public function delete($id)
    {
        return response()->json(BaseModel::find($id)->delete());
    }

    public function legalList(Request $request)
    {
        return BaseModel::getLegalsList($request);
    }
}
