<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 03.08.2018
 * Time: 10:27
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public function messages() {
        return [
            'required' => 'Заполните это поле',
            'numeric' => 'Это поле должно быть числовым',
            'min' => 'Не менее :min символа(-ов)',
            'max' => 'Не более :max символа(-ов)',
            'unique' => 'Уже используется',
            'email' => 'Введите правильный формат email',
        ];
    }
}