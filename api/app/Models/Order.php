<?php

namespace App\Models;

use App\Models\User\User;
use App\OrderJob;
use App\OrderPayment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    public function rules($request)
    {
        if ($request->isMethod('post')) {
            return [
                'execution_date' => ['required', function($attribute, $value, $fail){

                }],
                'customer' => 'required',
                'cost' => 'numeric|nullable',
                'address' => 'required',
                'payment_type' => 'required',
            ];
        }
        elseif ($request->isMethod('put')) {
            return [
                'execution_date' => ['required', function($attribute, $value, $fail){
                    $this->checkDispatcherAccess($fail);
                }],
                'customer' => 'required',
                'cost' => 'numeric|nullable',
                'address' => 'required',
                'payment_type' => 'required',
            ];
        }
    }

    public function checkDispatcherAccess($fail)
    {
        if (Auth::user()->hasRole('dispatcher')) {
            if ($this->user_id && $this->user_id != Auth::user()->id)
            {
                $fail('Заказ принадлежит другому диспетчеру');
            }
        }
    }
    protected $fillable = [
        'id',
        'customer',
        'address',
        'phone_number',
        'comment',
        'payment_type',
        'cost',
        'execution_date',
        'status',
        'status_updated_at',
    ];
    protected static function boot()
    {
        parent::boot();

        static::creating(function (Order $model) {
            $model->status_updated_at = Carbon::now()->toDateTimeString();
            self::setUserIdOnCreate($model);
        });

        static::created(function (Order $model) {
        });

        static::updating(function (Order $model) {
            if ($model->status != $model->getOriginal('status'))
            {
                $model->status_updated_at = Carbon::now()->toDateTimeString();
            }
            self::setUserIdOnUpdate($model);
        });

        static::updated(function (Order $model) {
        });
    }
    public static function search(Request $request)
    {
        $perpage = $request->get('pagination')['perpage'] ?? 10;
        $page = $request->get('pagination')['page'] ?? 1;
        if (isset($request->get('sort')['field'])) {
            $field = in_array($request->get('sort')['field'], (new Order())->fillable) ? $request->get('sort')['field'] : 'id';
        } else {
            $field = 'id';
        }
        $sort = $request->get('sort')['sort'] ?? 'desc';
        if ($request->get('query')) {

        }
        $models =  Order::when($request->get('orderBy'), function ($models) use ($request) {
                return $models
                    ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
            })
            ->when(Auth::user()->hasRole('dispatcher'), function($models) use ($request) {
                return $models->where('orders.user_id', '=', Auth::user()->id)->orWhere('orders.status', '=', 1);
            })
            ->when(isset($request->get('query')['id']), function($models) use ($request) {
                return $models->where('orders.id', '=', $request->get('query')['id']);
            })
            ->when(isset($request->get('query')['status']), function($models) use ($request) {
                return $models->where('orders.status', '=', $request->get('query')['status']);
            })
            ->when(isset($request->get('query')['customer']), function($models) use ($request) {
                return $models->where('orders.customer', 'LIKE', "%{$request->get('query')['customer']}%");
            })
            ->when(isset($request->get('query')['execution_date']), function($models) use ($request) {
                $startDate = Carbon::createFromFormat('Y-m-d', $request->get('query')['execution_date'])->hour(0)->minute(0)->second(0);
                $endDate = Carbon::createFromFormat('Y-m-d', $request->get('query')['execution_date'])->hour(23)->minute(59)->second(59);
                return
                    $models->where('orders.execution_date', '>', $startDate)
                        ->where('orders.execution_date', '<=', $endDate);
            })
            ->when(isset($request->get('query')['address']), function($models) use ($request) {
                return $models->where('orders.address', 'LIKE', "%{$request->get('query')['address']}%");
            })
            ->when(isset($request->get('query')['phone_number']), function($models) use ($request) {
                return $models->where('orders.phone_number', 'LIKE', "%{$request->get('query')['phone_number']}%");
            })
            ->when(isset($request->get('query')['comment']), function($models) use ($request) {
                return $models->where('orders.comment', 'LIKE', "%{$request->get('query')['comment']}%");
            })
            ->when($request->get('orderBy'), function ($users) use ($request, $field, $sort) {
                return $users
                    ->orderBy($field, $sort);
            })
            ->when(!$request->get('orderBy'), function ($users) use ($request, $field, $sort) {
                return $users
                    ->orderBy('id', 'desc');
            })
            ->with('jobs')
            ->with('user')
            ->with('payments')
            ->with('materials')
            ->with('jobs.performer');

        $count = $models->get()->count();
        $models = $models

            ->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request){
                return $models->skip($request->get('page') * 10)->take(10);
            })
            ->get();
        return response()->json([
            'models' => $models,
            'count' => $count
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        }
        else
        {
            $this->fill($request->all());
            if ($result = $this->save())
            {
                $this->saveJobs($request);
                $this->saveOrderMaterials($request);
                $this->savePayments($request);
                return response()->json($result, 200);
            }
            else
                return response()->json($result, 403);
        }
    }

    public static function setUserIdOnCreate(Order $model)
    {
        if (in_array($model->status, [3,4,5]))
        {
            $model->user_id = Auth::user()->id;
        }
    }

    public static function setUserIdOnUpdate(Order $model)
    {
        if (in_array($model->status, [2,3,4,5]))
        {
            $model->user_id = Auth::user()->id;
        }
    }

    public function messages() {
        return [
            'required' => 'Заполните это поле',
            'numeric' => 'Это поле должно быть числовым',
            'min' => 'Не менее :min символа(-ов)',
            'max' => 'Не более :max символа(-ов)',
            'unique' => 'Уже используется',
            'email' => 'Введите правильный формат email',
        ];
    }

    public function saveJobs(Request $request)
    {
        $jobsList = $request->get('jobs');
        foreach ($jobsList as $key => $value)
        {
            if ($orderJob = OrderJob::where([
                ['name' , '=', $key],
                ['order_id', '=', $this->id]
            ])->first())
            {
                $orderJob->fill($value);
                $orderJob->save();
            }
            else {
                $orderJob = new OrderJob();
                $orderJob->fill($value);
                $orderJob->name = $key;
                $orderJob->order_id = $this->id;
                $value['date'] ? $orderJob->date = Carbon::parse($value['date'])->toDateTimeString() : $orderJob->date = null;
                $orderJob->save();
            }
        }
    }

    public function saveOrderMaterials(Request $request)
    {
        $materials = $request->get('materials');
        OrderMaterial::where([
            ['order_id', '=', $this->id]
        ])->delete();
        foreach($materials as $material)
        {
            if ($material['material_id'] && $material['cost'] > 0)
            {
                $orderMaterial = new OrderMaterial();
                $orderMaterial->fill($material);
                $orderMaterial->order_id = $this->id;
                $orderMaterial->save();
            }
        }
    }

    public function savePayments(Request $request)
    {
        if (Auth::user()->hasRole('admin')){
            $payments = $request->get('payments');
            OrderPayment::where([
                ['order_id', '=', $this->id]
            ])->delete();
            foreach($payments as $payment)
            {
                if ($payment['payment'] && $payment['status'])
                {
                    $orderPayment = new OrderPayment();
                    $orderPayment->fill($payment);
                    $orderPayment->order_id = $this->id;
                    $orderPayment->save();
                }
            }
        }
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function jobs()
    {
        return $this->hasMany(OrderJob::class, 'order_id', 'id');
    }

    public function materials()
    {
        return $this->hasMany(OrderMaterial::class, 'order_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(OrderPayment::class, 'order_id', 'id');
    }

    public function setExecutionDateAttribute($value)
    {
        $this->attributes['execution_date'] = Carbon::parse($value)->toDateTimeString();
    }
}
