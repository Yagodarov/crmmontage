<?php

namespace App;

use App\Models\Performer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * @property int $id
 * @property int $performer_id
 * @property int $status
 * @property int $payment
 * @property string $date
 * @property string $created_at
 * @property string $updated_at
 * @property Performer $performer
 * @property Order $order
 */
class OrderJob extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['performer_id', 'order_id', 'name', 'status', 'payment', 'date', 'created_at', 'updated_at'];

    protected static $fillableByRole = [
        'admin' => ['performer_id', 'order_id', 'status', 'payment', 'date', 'created_at', 'updated_at'],
        'dispatcher' => ['performer_id', 'order_id', 'status', 'date', 'created_at', 'updated_at']
    ];

    public function __construct(array $attributes = [])
    {

        parent::__construct($attributes);
    }

    public function setDateAttribute($value)
    {
        $value ? $this->attributes['date'] = Carbon::parse($value)->toDateTimeString() : $this->attributes['date'] = null;
    }

    public function getFillableByRole()
    {
        $user = Auth::user();
        if ($user->hasRole('admin'))
        {
            $this->fillable = self::$fillableByRole['admin'];
        }
        elseif ($user->hasRole('dispatcher'))
        {
            $this->fillable = self::$fillableByRole['dispatcher'];
        }
    }

    static $jobTypes = [
        'measuring','calculation','sale', 'equipment','delivery','installation'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function performer()
    {
        return $this->belongsTo(Performer::class);
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
