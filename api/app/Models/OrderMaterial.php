<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $order_id
 * @property int $cost
 * @property string $created_at
 * @property string $updated_at
 * @property Order $order
 */
class OrderMaterial extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['order_id', 'cost', 'created_at', 'updated_at', 'material_id', 'count'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
