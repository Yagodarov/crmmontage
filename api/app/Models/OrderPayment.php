<?php

namespace App;

use App\Models\Performer;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $order_id
 * @property int $payment
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property Order $order
 */
class OrderPayment extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['order_id', 'payment', 'status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */


    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
