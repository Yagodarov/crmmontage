<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class Performer extends Model
{
    public function rules($request)
    {
        if ($request->isMethod('post')) {
            return [
                'first_name' => 'required',
                'surname' => 'required',
                'patronymic' => 'required',
                'phone' => 'required',
            ];
        }
        elseif ($request->isMethod('put')) {
            return [
                'first_name' => 'required',
                'surname' => 'required',
                'patronymic' => 'required',
                'phone' => 'required',
            ];
        }
    }
    protected $fillable = [
        'first_name', 'surname', 'patronymic',
        'phone'
    ];

    public static function search(Request $request)
    {
        $perpage = $request->get('pagination')['perpage'] ?? 10;
        $page = $request->get('pagination')['page'] ?? 1;
        $fillable = (new Order())->getFillable();
        if (isset($request->get('sort')['field'])) {
            $field = in_array($request->get('sort')['field'], $fillable) ? $request->get('sort')['field'] : 'id';
        } else {
            $field = 'id';
        }
        $sort = $request->get('sort')['sort'] ?? 'desc';
        $globalSearch = isset($request->get('query')['generalSearch']) ? $request->get('query')['generalSearch'] : '';
        $models =  DB::table('performers')
            ->select('*')
            ->when($request->get('orderBy'), function ($users) use ($request) {
                return $users
                    ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
            })
            ->when($globalSearch, function($models) use ($globalSearch){
                return $models
                    ->orWhere('performers.phone', 'LIKE', "%{$globalSearch}%")
                    ->orWhere('performers.first_name', 'LIKE', "%{$globalSearch}%")
                    ->orWhere('performers.surname', 'LIKE', "%{$globalSearch}%")
                    ->orWhere('performers.patronymic', 'LIKE', "%{$globalSearch}%");
            })
            ->when(!$request->get('orderBy'), function ($users) use ($request, $field, $sort) {
                return $users
                    ->orderBy($field, $sort);
            });

        $count = $models->get()->count();
        $models = $models

            ->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request){
                return $models->skip($request->get('page') * 10)->take(10);
            })
            ->get();
        return response()->json([
            'models' => $models,
            'count' => $count
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        }
        else
        {
            $this->fill($request->all());
            if ($result = $this->save())
            {
                return response()->json($result, 200);
            }
            else
                return response()->json($result, 403);
        }
    }

    public function storeUpdate(Request $request)
    {
        $validate = Validator::make($request->all(), $this->rules($request), $this->messages());
        if (!$validate->fails())
        {
            $this->fill($request->all());
            if ($result = $this->save())
            {
                return response()->json($result, 200);
            }
            else
            {
                return response()->json($result, 403);
            }
        }
        else
        {
            return response()->json($validate->errors(), 403);
        }
    }

    public function messages() {
        return [
            'required' => 'Заполните это поле',
            'min' => 'Не менее :min символа(-ов)',
            'max' => 'Не более :max символа(-ов)',
            'unique' => 'Уже используется',
            'email' => 'Введите правильный формат email',
        ];
    }
}
