<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->text('customer')->nullable();
            $table->text('address')->nullable();
            $table->string('phone_number')->nullable();
            $table->text('comment')->nullable();
            $table->integer('payment_type')->default(0)->nullable();
            $table->integer('cost')->nullable();
            $table->dateTimeTz('execution_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
