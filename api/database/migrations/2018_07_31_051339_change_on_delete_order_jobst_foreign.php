<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOnDeleteOrderJobstForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_jobs', function (Blueprint $table) {
            $table->dropForeign('order_jobs_performer_id_foreign');
            $table->foreign('performer_id')->references('id')->on('performers')->onDelete('set null')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_jobs', function (Blueprint $table) {
            $table->dropForeign('order_jobs_performer_id_foreign');
            $table->foreign('performer_id')->references('id')->on('performers')->onDelete('cascade')->change();
        });
    }
}
