<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.12.2017
 * Time: 22:10
 */
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RPSeeder extends Seeder
{
    public function run(){
        app()['cache']->forget('spatie.permission.cache');

        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('roles')->truncate();
        DB::table('permissions')->truncate();
        DB::table('role_has_permissions')->truncate();

        $admin = Role::create(['name' => 'admin', 'title' => 'Администратор']);
        $dispatcher = Role::create(['name' => 'dispatcher', 'title' => 'Диспетчер']);

        $manage_users = Permission::create(['name' => 'manage_users']);
        $manage_performers = Permission::create(['name' => 'manage_performers']);
        $get_performers = Permission::create(['name' => 'get_performers']);
        $manage_orders = Permission::create(['name' => 'manage_orders']);
        $delete_orders = Permission::create(['name' => 'delete_orders']);
        $manage_materials = Permission::create(['name' => 'manage_materials']);

        $manage_materials->assignRole($admin);
        $manage_users->assignRole($admin);
        $manage_performers->assignRole($admin);
        $manage_orders->assignRole($admin, $dispatcher);
        $delete_orders->assignRole($admin);
        $get_performers->assignRole($admin, $dispatcher);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}