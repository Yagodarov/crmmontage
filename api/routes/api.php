<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware([
    'prefix' => 'api'
])->group(function() {

    Route::post('authenticate', 'Auth\LoginController@authenticate');
    Route::get('verify', 'Auth\LoginController@verify');

    Route::get('users', 'UserController@index')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::post('user', 'UserController@store')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::get('user/{id}', 'UserController@get')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::put('user', 'UserController@update')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::put('user/block/{id}', 'UserController@block')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::delete('user/{id}', 'UserController@delete')->middleware(['jwt.auth', 'permission:manage_users']);

    Route::get('materials', 'MaterialController@index');
    Route::post('material', 'MaterialController@store');
    Route::get('material/{id}', 'MaterialController@get');
    Route::put('material', 'MaterialController@update');
    Route::delete('material/{id}', 'MaterialController@delete')->middleware(['jwt.auth', 'permission:manage_materials']);

    Route::get('orders', 'OrderController@index')->middleware(['jwt.auth', 'permission:manage_orders']);
    Route::post('order', 'OrderController@store')->middleware(['jwt.auth', 'permission:manage_orders']);
    Route::get('order/{id}', 'OrderController@get')->middleware(['jwt.auth', 'permission:manage_orders']);
    Route::put('order', 'OrderController@update')->middleware(['jwt.auth', 'permission:manage_orders']);
    Route::delete('order/{id}', 'OrderController@delete')->middleware(['jwt.auth', 'permission:delete_orders']);

    Route::get('performers', 'PerformerController@index')->middleware(['jwt.auth', 'permission:get_performers']);
    Route::post('performer', 'PerformerController@store')->middleware(['jwt.auth', 'permission:manage_performers']);
    Route::get('performer/{id}', 'PerformerController@get')->middleware(['jwt.auth', 'permission:manage_performers']);
    Route::put('performer', 'PerformerController@update')->middleware(['jwt.auth', 'permission:manage_performers']);
    Route::delete('performer/{id}', 'PerformerController@delete')->middleware(['jwt.auth', 'permission:manage_performers']);
});