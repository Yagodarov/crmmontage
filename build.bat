@echo off
echo.
if "%~1"=="" GOTO UNKNOWN
del *bundle.js*
del *chunk.js*
call ng build --prod
set commitMessage=%1
ROBOCOPY dist . /s /e /it
rmdir /s /q dist
git add .
git commit -m %commitMessage%
git push

GOTO DONE

:UNKNOWN
Echo Error no commit comment	
goto:eof

:DONE
ECHO Done!
goto:eof