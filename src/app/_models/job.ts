export class Job {
    status;
    performer;
    payment;
    date;
    jobNames = function(){
        return [
            'measuring',
            'calculation',
            'sale',
            'equipment',
            'delivery',
            'installation'
        ]};
}