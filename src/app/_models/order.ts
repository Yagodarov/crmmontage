export class Order {
    constructor(){

    }
    getJobTypesList()
    {
        return [
            {
                'type' : 'measuring',
                'label' : 'Замер'
            },{
                'type' : 'calculation',
                'label' : 'Расчет'
            },{
                'type' : 'sale',
                'label' : 'Продажа'
            },{
                'type' : 'equipment',
                'label' : 'Комплектация'
            },{
                'type' : 'delivery',
                'label' : 'Доставка'
            },{
                'type' : 'installation',
                'label' : 'Монтаж'
            },
        ];
    }
    getOrderStatusList(){
        return [
            {
                id : 1,
                label : 'Новый'
            },
            {
                id : 2,
                label : 'В обработке'
            },
            {
                id : 3,
                label : 'В работе'
            },
            {
                id : 4,
                label : 'Отказ'
            },
            {
                id : 5,
                label : 'Выполнен'
            },
        ];
    }
    getJobStatusList(){
        return [
            {
                id : 1,
                label : 'Назначен'
            },
            {
                id : 2,
                label : 'На исполнении'
            },
            {
                id : 3,
                label : 'Выполнен'
            },
        ];
    }
    getPaymentStatusList(){
        return [
            {
                id : 1,
                label : 'Наличные'
            },
            {
                id : 2,
                label : 'Безналичные'
            },
        ];
    }
}