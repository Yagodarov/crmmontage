/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve, ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {UserService} from "../_services/user.service";

@Injectable()
export class UserEditResolver implements Resolve<any>{
  constructor(private service: UserService,private activatedRoute: ActivatedRoute) {
  }
  resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<any> {
    return this.service.getById(route.queryParams.id);
  }

}