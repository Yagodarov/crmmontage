import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class MaterialService {
    url = 'material';
    constructor(private http: HttpClient) { }

    getAll(params= {}) {
        return this.http.get<any[]>('/api/'+this.url + 's', params);
    }

    getById(id: number) {
        return this.http.get<any>('/api/'+this.url + '/' + id);
    }

    create(user: any) {
        return this.http.post('/api/'+this.url, user);
    }

    update(user: any) {
        return this.http.put('/api/'+this.url, user);
    }

    delete(id: number) {
        return this.http.delete('/api/'+this.url + '/' + id);
    }
}
