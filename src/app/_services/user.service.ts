import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll(params= {}) {
        return this.http.get<any[]>('/api/users', params);
    }

    getById(id: number) {
        return this.http.get<any>('/api/user/' + id);
    }

    create(user: any) {
        return this.http.post('/api/user', user);
    }

    block(user: any) {
        return this.http.put('/api/user/block/' + user.id, user);
    }

    update(user: any) {
        return this.http.put('/api/user', user);
    }

    delete(id: number) {
        return this.http.delete('/api/user/' + '/' + id);
    }
}
