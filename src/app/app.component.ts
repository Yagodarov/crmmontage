import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router, NavigationStart, NavigationEnd} from '@angular/router';
import {Helpers} from "./helpers";
declare var Modernizr : any;
declare var $ : any;
@Component({
	selector: 'body',
	templateUrl: './app.component.html',
	encapsulation: ViewEncapsulation.None,
    styleUrls: [
    	"../assets/main.css"
	]
})
export class AppComponent implements OnInit {
	title = 'app';
	globalBodyClass = 'm-page--loading-non-block m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default m-brand--minimize m-aside-left--minimize';

	constructor(private _router: Router) {
        Helpers.bodyClass(this.globalBodyClass);
	}

	ngOnInit() {
		this._router.events.subscribe((route) => {
			// console.log(route);
			if (route instanceof NavigationStart) {
				Helpers.setLoading(true);
			}
			if (route instanceof NavigationEnd) {
				Helpers.setLoading(false);
				this.initModernizr();
			}
		});
	}

	initModernizr()
	{
        if(!Modernizr.inputtypes['datetime-local']) {
            setTimeout(() => {
                $('input[type=datetime-local]').datetimepicker();
            }, 100);
        }
	}
}