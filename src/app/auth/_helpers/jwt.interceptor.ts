import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private router: Router) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // add authorization header with jwt token if available
        let user = JSON.parse(localStorage.getItem('currentUser'));
        console.log(user);
        if (user && user.token) {
            request = request.clone({
                setHeaders: { 
                    Authorization: `Bearer ${user.token}`
                }
            });
            // request.headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            // request.headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
            // request.headers.append('Access-Control-Allow-Credentials', 'true');
        }

        return next.handle(request).do(event => {}, err => {
            if (err instanceof HttpErrorResponse && (err.status == 401 || err.status == 400)) {
                localStorage.clear();
                this.router.navigate(['login']);
            }
        });
    }
}