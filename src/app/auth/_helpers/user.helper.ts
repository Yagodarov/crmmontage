import {Injectable} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

@Injectable()
export class UserHelper {
    constructor(private router: Router) {

    }

    getUserName() {
        var storage = JSON.parse(localStorage.getItem('currentUser'));
        if (storage && storage.user)
            return storage.user.username;
        else
            return false;
    }

    getUserId() {
        var storage = JSON.parse(localStorage.getItem('currentUser'));
        if (storage && storage.user)
            return storage.user.id;
        else
            return false;
    }


    logout() {
        localStorage.clear();
        this.router.navigate([this.getLoginUrl()]);
    }

    getUserRole() {
        var storage = JSON.parse(localStorage.getItem('currentUser'));
        if (storage && storage.role)
            return storage.role;
        else
            return false;
    }

    getNavBar() {

    }

    navigateAfterLogin() {
        switch (this.getUserRole()) {
            case 'manager' : {
                window.open('/parking', '_blank');
            }
        }
    }

    getStartUrl() {
        return this.getReturnUrl();
    }

    getLoginUrl() {
        return 'login';
    }

    getReturnUrl() {
        switch (this.getUserRole()) {
            case 'admin':
                return 'users';
            case 'manager' :
                return 'orders';
            case 'operator' :
                return 'parking';
            case 'terminal_user' :
                return 'terminal';
        }
    }
}