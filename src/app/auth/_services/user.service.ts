import {Injectable} from "@angular/core";
import { HttpClient} from '@angular/common/http';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders} from '@angular/common/http';
import {User} from '../_models';

@Injectable()
export class UserService {
	constructor(private http: HttpClient) {
	}

	authenticate(data : User) {
		return this.http.post('/api/authenticate', data);
	}
    verify() {
		return this.http.get('/api/verify');
	}

	forgotPassword(email: string) {
		return this.http.post('/api/forgot-password', JSON.stringify({email}), this.jwt()).map((response: any) => response.json());
	}

	getAll() {
		return this.http.get('/api/users', this.jwt()).map((response: any) => response.json());
	}

	getById(id: number) {
		return this.http.get('/api/users/' + id, this.jwt()).map((response: any) => response.json());
	}

	create(user: User) {
		return this.http.post('/api/users', user, this.jwt()).map((response: any) => response.json());
	}

	update(user: User) {
		return this.http.put('/api/users/' + user.id, user, this.jwt()).map((response: any) => response.json());
	}

	delete(id: number) {
		return this.http.delete('/api/users/' + id, this.jwt()).map((response: any) => response.json());
	}

	// private helper methods

	private jwt() {
		// create authorization header with jwt token
		let currentUser = JSON.parse(localStorage.getItem('currentUser'));
		if (currentUser && currentUser.token) {
			let headers = new HttpHeaders({'Authorization': 'Bearer ' + currentUser.token});
			return {headers: headers};
		}
	}
}