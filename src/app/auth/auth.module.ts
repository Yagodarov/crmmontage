import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthRoutingModule} from './auth-routing.routing';
import {AuthComponent} from './auth.component';
import {AlertComponent} from './_directives';
import {LogoutComponent} from './logout/logout.component';
import {AuthGuard} from './_guards';
import {AlertService} from './_services';
import {AuthenticationService} from './_services';
import {UserService} from './_services';

@NgModule({
    declarations: [
        AuthComponent,
        AlertComponent,
        LogoutComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        AuthRoutingModule,
        HttpClientModule,
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
    ],
    entryComponents: [AlertComponent],
})

export class AuthModule {
}