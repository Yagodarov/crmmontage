import {Component, OnInit, ViewEncapsulation, AfterViewInit} from '@angular/core';
import {Helpers} from '../../../helpers';

declare let mLayout: any;

@Component({
    selector: 'app-aside-nav',
    templateUrl: './aside-nav.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class AsideNavComponent implements OnInit, AfterViewInit {
    role : any;

    constructor() {

    }

    ngOnInit() {
        this.getRole();
    }

    getRole()
    {
        this.role = JSON.parse(localStorage.getItem('currentUser'))['role'];
        console.log(this.role);
    }

    ngAfterViewInit() {
        var defaultOptions = {
            // autoscroll on accordion submenu tog
            autoscroll: {
                speed: 1200
            },

            // accordion submenu mode
            accordion: {
                slideSpeed: 200, // accordion toggle slide speed in milliseconds
                autoScroll: false, // enable auto scrolling(focus) to the clicked menu item
                autoScrollSpeed: 1200,
                expandAll: true // allow having multiple expanded accordions in the menu
            },

            // dropdown submenu mode
            dropdown: {
                timeout: 500 // timeout in milliseconds to show and hide the hoverable submenu dropdown
            }
        };
        mLayout.scrollTop = 0;
        mLayout.initAside(defaultOptions);
    }

}