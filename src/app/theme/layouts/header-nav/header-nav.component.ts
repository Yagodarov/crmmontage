import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import {UserHelper} from '../../../auth/_helpers/user.helper';

declare let mLayout: any;
@Component({
selector: "app-header-nav",
templateUrl: "./header-nav.component.html",
encapsulation: ViewEncapsulation.None,
    providers: [UserHelper]
})
export class HeaderNavComponent implements OnInit, AfterViewInit {
username = '';

constructor(user : UserHelper)  {
    this.username = user.getUserName();
}
ngOnInit()  {

}
ngAfterViewInit()  {

mLayout.initHeader();

}

}