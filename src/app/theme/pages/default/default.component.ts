import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Helpers} from '../../../helpers';
import {ScriptLoaderService} from '../../../_services/script-loader.service';
@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-grid.m-grid--ver-desktop.m-grid--desktop.m-body',
    templateUrl: './default.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class DefaultComponent implements OnInit, AfterViewInit {


    constructor(private _script: ScriptLoaderService) {

    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        this._script.loadScripts('.m-grid__item.m-grid__item--fluid.m-grid.m-grid--ver-desktop.m-grid--desktop.m-body',
            ['assets/jquery.scrollTo.min.js']);
    }
}