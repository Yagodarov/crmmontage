import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {MaterialService as Service} from '../../../../../_services/material.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';

@Component({
  selector: 'app-material-create',
  templateUrl: './material-create.component.html',
  styles: []
})
export class MaterialCreateComponent implements OnInit {
    url = 'materials';
    item = 'Материал';
    errors = [];
    form: FormGroup;

    constructor(private _fb: FormBuilder, private _location: Location,
                private _service: Service,
                private _titleService: Title,
                private router: Router
    ) {
        this.initForm();
    }

    initForm() {
        this.form = this._fb.group({
            name: '',
            cost: '',
        });
    }

    submitForm() {
        this._service.create(this.form.value).subscribe((data) => {
            this.router.navigate(['/' + this.url]);
        }, (data) => {
            this.errors = data['error'];
            this.scrollToError();
        });
        console.log(this.form);
    }

    scrollToError() {
        var id = '#' + Object.keys(this.errors)[0] + 'Input';
        $('html, body').animate({scrollTop: $(id).position().top - 170}, 400);
    }

    cancelCreate() {
        this._location.back();
    }

    ngOnInit() {
        this.setTitle();
    }

    getInputClass(field) {
        let classList = {
            'has-danger': this.errors[field] != undefined ? true : false
        };
        return classList;
    }

    setTitle() {
        this._titleService.setTitle( 'Добавить '+this.item );
    }
}
