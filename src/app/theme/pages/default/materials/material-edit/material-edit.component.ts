import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {MaterialService as Service} from '../../../../../_services/material.service';

@Component({
  selector: 'app-material-edit',
    templateUrl: './material-edit.component.html',
  styles: []
})
export class MaterialEditComponent implements OnInit {
    url = 'materials';
    item = 'Материал';
    errors = [];
    form: FormGroup;

    constructor(private _fb: FormBuilder, private _location: Location,
                private _service: Service,
                private _titleService: Title,
                private route: ActivatedRoute,
                private router: Router
    ) {
        this.initForm();
    }

    initForm() {
        this.form = this._fb.group({
            id: '',
            name: '',
            cost: '',
        });
    }

    submitForm() {
        this._service.update(this.form.value).subscribe((data) => {
            this.router.navigate(['/' + this.url]);
        }, (data) => {
            this.errors = data['error'];
            this.scrollToError();
        });
        console.log(this.form);
    }

    scrollToError() {
        var id = '#' + Object.keys(this.errors)[0] + 'Input';
        $('html, body').animate({scrollTop: $(id).position().top - 170}, 400);
    }

    cancelCreate() {
        this._location.back();
    }

    ngOnInit() {
        this.initFormValues();
        this.setTitle();
    }
    initFormValues() {
        this.route.data
            .subscribe((data) => {
                console.log(data);
                this.form.patchValue(data['data'], {
                    emitEvent : false,
                    onlySelf : true
                });
            });
    }
    getInputClass(field) {
        let classList = {
            'has-danger': this.errors[field] != undefined ? true : false
        };
        return classList;
    }

    setTitle() {
        this._titleService.setTitle( 'Добавить '+this.item );
    }
}
