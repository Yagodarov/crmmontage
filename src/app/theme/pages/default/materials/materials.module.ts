import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialCreateComponent } from './material-create/material-create.component';
import { MaterialsComponent } from './materials/materials.component';
import { MaterialEditComponent } from './material-edit/material-edit.component';
import {DefaultComponent} from '../default.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialService} from '../../../../_services/material.service';
import {MaterialEditResolver} from '../../../../_resolvers/material-edit';
import {LayoutModule} from '../../../layouts/layout.module';
const routes: Routes = [
    {
        'path': '',
        "component": DefaultComponent,
        'children': [
            {
                path: '',
                component: MaterialsComponent
            },
            {
                path: 'new',
                component: MaterialCreateComponent,
            },
            {
                path: 'edit',
                component: MaterialEditComponent,
                resolve : {
                    data : MaterialEditResolver
                }
            },

        ]
    }
];
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, ReactiveFormsModule
  ],
    exports : [
        RouterModule, FormsModule, ReactiveFormsModule
    ],
  declarations: [MaterialCreateComponent, MaterialsComponent, MaterialEditComponent],
    providers: [
        MaterialService, MaterialEditResolver
    ]
})
export class MaterialsModule { }
