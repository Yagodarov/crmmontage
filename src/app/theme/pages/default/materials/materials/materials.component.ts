import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {MaterialService} from '../../../../../_services/material.service';
import {UserHelper} from '../../../../../auth/_helpers/user.helper';

@Component({
  selector: 'app-materials',
    templateUrl: './materials.component.html',
  styles: [],
    providers: [UserHelper]
})
export class MaterialsComponent implements OnInit {
    url = 'materials';
    title = 'Материалы';
    itemTitle = 'Материал';
    itemsTitle = 'Материалов';
    datatable : any;
    constructor(private router: Router, private _titleService: Title, private _service: MaterialService, private user: UserHelper) {
        window['angular'] = this;
    }

    ngOnInit() {
        this.datatable = $(".items").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: 'api/'+this.url,
                        method: 'GET',
                        // custom headers
                        headers: {
                            'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser'))['token']
                        },
                        map: function (t) {
                            var e = t;
                            return void 0 !== t.models && (e = t.models), e
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {scroll: !1, footer: !1},
            sortable: !0,
            pagination: !0,
            toolbar: {items: {pagination: {pageSizeSelect: [10, 20, 30, 50, 100]}}},
            search: {input: $("#generalSearch")},
            columns: [
                {
                    field: "id",
                    title: "#",
                    sortable: !1,
                    width: 40,
                    selector: !1,
                    textAlign: "center"
                },
                {
                    field: 'name',
                    title: 'Название',
                    sortable: 'asc',
                    overflow: 'visible',
                    width: 140,
                },
                {
                    field: 'cost',
                    title: 'Цена',
                    sortable: 'asc',
                    overflow: 'visible',
                    width: 140,
                },
                {
                    field: 'Actions',
                    width: 110,
                    title: 'Действия',
                    sortable: 'asc',
                    overflow: 'visible',
                    template: (t, a, e) => {
                        var html = '<a class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" onclick="angular.edit('+t.id+')" title="Edit"><i class="la la-edit"></i></a>';
                        if (this.user.getUserRole() === 'admin')
                        {
                            html += '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="angular.delete('+t.id+')" title="Delete"><i class="la la-trash"></i></a>';
                        }
                        return html;
                    }
                },
            ]
        });
        this.datatable.on('m-datatable--on-ajax-fail', ($param) => {
            this.user.logout();
        });
        this.setTitle();
    }

    edit(item_id)
    {
        this.router.navigate([this.url+'/edit'], {queryParams: {'id' : item_id}});
    }

    delete(item_id)
    {
        if (confirm('Вы уверены?'))
        {
            this._service.delete(item_id).subscribe(() => {
                this.datatable.reload();
            }, () => {
                alert('Не удалось удалить');
            })
        }
    }

    setTitle() {
        this._titleService.setTitle( this.title );
    }

}
