import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {OrderService } from '../../../../../../_services/order.service';
import {Location} from '@angular/common';
import {Title} from '@angular/platform-browser';
import {Order} from '../../../../../../_models/order';
import {Price} from '../../../../../../_models/price';
import * as moment from 'moment';
import {Job} from '../../../../../../_models/job';
@Component({
  selector: 'app-order-create-dispatcher',
    templateUrl: './order-create-dispatcher.component.html'
})
export class OrderCreateDispatcherComponent implements OnInit {
    order : Order = new Order;
    price : Price = new Price;
    job : Job = new Job;
    returnUrl = 'orders';
    title = 'Добавить заказ';
    button_label = 'Добавить заказ';
    payments : any = [];
    errors = [];
    performersList = [];
    materialsList = [];
    types_of_jobs = [];
    form: FormGroup;

    constructor(private _fb: FormBuilder, private _location: Location,
                private _service: OrderService,
                private _titleService: Title,
                private router: Router,
                private route: ActivatedRoute
    ) {
        this.types_of_jobs = this.order.getJobTypesList();
        this.resolveRoute();
        this.initForm();
    }

    resolveRoute()
    {
        this.route.data.subscribe((data) => {
            this.performersList.push({
                id : null,
                first_name : '',
                surname : "",
                patronymic : '--Исполнитель--'
            });
            data['performers']['models'].forEach((item) => {
                this.performersList.push(item);
            });
            this.materialsList.push({
                id : null,
                name : '--Материал--'
            });
            data['materials']['models'].forEach((item) => {
                this.materialsList.push(item);
            });
        })
    }

    initForm() {
        this.form = this._fb.group({
            customer: '',
            execution_date: moment().format("YYYY-MM-DD[T]HH:mm"),
            address: '',
            phone_number: '',
            comment: '',
            payment_type: '',
            cost: '',
            status: 1,
            jobs : this._fb.group({
                'measuring' : this.createJobItem(),
                'calculation' : this.createJobItem(),
                'sale' : this.createJobItem(),
                'equipment' : this.createJobItem(),
                'delivery' : this.createJobItem(),
                'installation' : this.createJobItem(),
            }),
            materials: this._fb.array([this.createMaterialItem()])
        });
    }

    createJobItem(): FormGroup {
        let thisFormGroup =  this._fb.group({
            status: new FormControl(''),
            performer_id : null,
            payment : '',
            date : '',
        });
        thisFormGroup.controls['status'].valueChanges.subscribe((value) => {
            thisFormGroup.controls['date'].setValue(moment().toISOString());
        });
        thisFormGroup.controls['performer_id'].valueChanges.subscribe((value) => {
            thisFormGroup.controls['date'].setValue(moment().toISOString());
        });
        thisFormGroup.controls['payment'].valueChanges.subscribe((value) => {
            thisFormGroup.controls['date'].setValue(moment().toISOString());
        });
        return thisFormGroup;
    }

    createMaterialItem():FormGroup {
        const thisFormGroup =  this._fb.group({
            material_id: null,
            cost: '',
        });
        thisFormGroup.controls['material_id'].valueChanges.subscribe((value) => {
            let material;
            if (this.materialsList.length > 0)
            {
                material = this.materialsList.find(x => x.id === value)
            }
            console.log(material);
            console.log(thisFormGroup);
            thisFormGroup.controls['cost'].setValue(material.cost);
        });
        return thisFormGroup;
    }

    addMaterialItem():void {
        let materials = this.form.get('materials') as FormArray;
        materials.push(this.createMaterialItem());
    }

    removeMaterialItem(id):void {
        let materials = this.form.get('materials') as FormArray;
        if (materials.length > 1)
            materials.removeAt(id);
    }

    submitForm() {
        let submitModel = this.form.value;
        submitModel.execution_date = moment(submitModel.execution_date).toISOString();
        this._service.create(this.form.value).subscribe((data) => {
            this.router.navigate(['/' + this.returnUrl]);
        }, (data) => {
            this.errors = data['error'];
            this.scrollToError();
        });
    }

    scrollToError() {
        var id = '#' + Object.keys(this.errors)[0] + 'Input';
        $('html, body').animate({scrollTop: $(id).offset().top - 80}, 400);
    }

    cancelCreate() {
        this._location.back();
    }

    ngOnInit() {
        this.setTitle();
    }

    getInputClass(field) {
        let classList = {
            'has-danger': this.errors[field] != undefined ? true : false
        };
        return classList;
    }

    setTitle() {
        this._titleService.setTitle( this.title );
    }

    totalPrice(item)
    {
        if (item.controls['count'].value && item.controls['cost'].value)
        {
            return item.controls['count'].value * item.controls['cost'].value;
        }
        else
            return '';
    }
}
