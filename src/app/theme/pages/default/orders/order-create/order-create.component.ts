import {Component, OnInit} from '@angular/core';
import {UserHelper} from '../../../../../auth/_helpers/user.helper';
@Component({
  selector: 'app-order-create',
    templateUrl: './order-create.component.html',
  styleUrls: [
      'order-create.component.css'
  ],
    providers: [UserHelper]
})
export class OrderCreateComponent implements OnInit {
    user_role;

    constructor(private user : UserHelper) {
    }

    ngOnInit() {
        this.user_role = this.user.getUserRole();
    }
}
