import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {OrderService } from '../../../../../../_services/order.service';
import {Location} from '@angular/common';
import {Title} from '@angular/platform-browser';
import * as moment from 'moment';
import {Order} from '../../../../../../_models/order';
import {Price} from '../../../../../../_models/price';
import {Job} from '../../../../../../_models/job';

@Component({
    selector: 'app-order-edit-dispatcher',
    templateUrl: './order-edit-dispatcher.component.html',
    styles: []
})
export class OrderEditDispatcherComponent implements OnInit {
    order : Order = new Order;
    price : Price = new Price;
    job : Job = new Job;
    returnUrl = 'orders';
    title = 'Редактировать заказ';
    button_label = 'Редактировать заказ';
    payments : any = [];
    errors = [];
    types_of_jobs = [];
    performersList = [];
    materialsList = [];
    form: FormGroup;

    constructor(private _fb: FormBuilder, private _location: Location,
                private _service: OrderService,
                private _titleService: Title,
                private route: ActivatedRoute,
                private router: Router
    ) {
        this.types_of_jobs = this.order.getJobTypesList();
    }

    initForm() {
        this.form = this._fb.group({
            id : '',
            customer: '',
            execution_date: '',
            address: '',
            phone_number: '',
            comment: '',
            payment_type: '',
            cost: '',
            status: '',
            jobs : this._fb.group({
                'measuring' : this.createJobItem(),
                'calculation' : this.createJobItem(),
                'sale' : this.createJobItem(),
                'equipment' : this.createJobItem(),
                'delivery' : this.createJobItem(),
                'installation' : this.createJobItem(),
            })
        });
    }

    resolveRoute()
    {
        this.route.data.subscribe((data) => {
            this.performersList.push({
                id : null,
                first_name : '',
                surname : "",
                patronymic : '--Исполнитель--'
            });
            data['performers']['models'].forEach((item) => {
                this.performersList.push(item);
            });
            this.materialsList.push({
                id : null,
                name : '--Материал--'
            });
            data['materials']['models'].forEach((item) => {
                this.materialsList.push(item);
            });
        })
    }

    createJobItem(): FormGroup {
        return this._fb.group({
            status: '',
            performer_id : null,
            payment : '',
            date : '',
        });
    }

    createMaterialItem():FormGroup {
        const thisFormGroup =  this._fb.group({
            material_id: null,
            cost: '',
        });
        thisFormGroup.controls['material_id'].valueChanges.subscribe((value) => {
            let material;
            if (this.materialsList.length > 0)
            {
                material = this.materialsList.find(x => x.id === value)
            }
            console.log(material);
            console.log(thisFormGroup);
            thisFormGroup.controls['cost'].setValue(material.cost);
        });
        return thisFormGroup;
    }

    addMaterialItem():void {
        let materials = this.form.get('materials') as FormArray;
        materials.push(this.createMaterialItem());
    }

    removeMaterialItem(id):void {
        let materials = this.form.get('materials') as FormArray;
        if (materials.length > 1)
            materials.removeAt(id);
    }

    submitForm() {
        let submitModel = this.form.value;
        submitModel.execution_date = moment(submitModel.execution_date).toISOString();
        this.dateValuesToIso(submitModel);
        this._service.update(submitModel).subscribe((data) => {
            this.router.navigate(['/' + this.returnUrl]);
        }, (data) => {
            this.errors = data['error'];
            this.scrollToError();
        });
    }

    scrollToError() {
        var id = '#' + Object.keys(this.errors)[0] + 'Input';
        $('html, body').animate({scrollTop: $(id).offset().top - 80}, 400);
    }

    cancelCreate() {
        this._location.back();
    }

    ngOnInit() {
        this.initForm();
        this.resolveRoute();
        this.initFormValues();
        this.setTitle();
    }
    initFormValues() {
        this.route.data
            .subscribe((data) => {
                this.setFormValue(data);
                this.setUtcDate(data);
                this.setJobsValue(data);
                this.setMaterialsValue(data);
            });
    }

    setMaterialsValue(data)
    {
        if (data.data.materials.length)
        {
            let items = [];
            data.data.materials.forEach((material) => {
                this.payments = this.form.get('materials') as FormArray;
                items.push(this._fb.group({
                    material_id: material.material_id,
                    cost : material.cost
                }));
            });
            this.form.addControl('materials',this._fb.array(items));
        }
        else
        {
            this.form.addControl('materials',this._fb.array([this.createMaterialItem()]));
        }
    }

    setFormValue(data)
    {
        this.form.patchValue(data['data'], {
            emitEvent : false,
            onlySelf : true
        });
    }

    setJobsValue(data)
    {
        data['data']['jobs'].forEach((job) => {
            this.form.controls['jobs']['controls'][job['name']].controls['status'].setValue(job['status'], { emitEvent : false });
            this.form.controls['jobs']['controls'][job['name']].controls['performer_id'].setValue(job['performer_id'], { emitEvent : false });
            this.form.controls['jobs']['controls'][job['name']].controls['payment'].setValue(job['payment'], { emitEvent : false });

            if (job['date'])
            {
                var stillUtc = moment.utc(job['date']).toDate();
                var local = moment(stillUtc).local().format('YYYY-MM-DD[T]HH:mm:ss');
                this.form.controls['jobs']['controls'][job['name']].controls['date'].setValue(local, { emitEvent : false });
            }
        });
    }

    setUtcDate(data)
    {
        var stillUtc = moment.utc(data['data']['execution_date']).toDate();
        var local = moment(stillUtc).local().format('YYYY-MM-DD[T]HH:mm');
        this.form.controls['execution_date'].setValue(local);
    }

    dateValuesToIso(submitModel)
    {
        this.job.jobNames().forEach((item) => {
            submitModel.jobs[item]['date'] =  moment(submitModel.jobs[item]['date']).toISOString();
        });

    }

    getInputClass(field) {
        let classList = {
            'has-danger': this.errors[field] != undefined ? true : false
        };
        return classList;
    }

    setTitle() {
        this._titleService.setTitle( this.title );
    }

    compare(c1, c2) {
        return true;
    }

    totalPrice(item)
    {
        if (item.controls['count'].value && item.controls['cost'].value)
        {
            return item.controls['count'].value * item.controls['cost'].value;
        }
        else
            return '';
    }
}
