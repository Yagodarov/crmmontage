import { Component, OnInit } from '@angular/core';
import {UserHelper} from '../../../../../auth/_helpers/user.helper';

@Component({
  selector: 'app-order-edit',
    templateUrl: './order-edit.component.html',
  styles: [],
    providers: [UserHelper]
})
export class OrderEditComponent implements OnInit {
    user_role;

    constructor(private user : UserHelper) {
    }

    ngOnInit() {
        this.user_role = this.user.getUserRole();
    }
}
