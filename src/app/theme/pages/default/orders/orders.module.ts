import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersComponent } from './orders/orders.component';
import { OrderCreateComponent } from './order-create/order-create.component';
import { OrderEditComponent } from './order-edit/order-edit.component';
import {DefaultComponent} from '../default.component';
import {RouterModule, Routes} from '@angular/router';
import {OrderEditResolver} from '../../../../_resolvers/order-edit';
import {OrderService} from '../../../../_services/order.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LayoutModule} from '../../../layouts/layout.module';
import {PerformersListResolver} from '../../../../_resolvers/performers-list.service';
import {PerformerService} from '../../../../_services/performer.service';
import { OrdersAdminComponent } from './orders/orders-admin/orders-admin.component';
import { OrdersDispatcherComponent } from './orders/orders-dispatcher/orders-dispatcher.component';
import { OrderCreateAdminComponent } from './order-create/order-create-admin/order-create-admin.component';
import { OrderCreateDispatcherComponent } from './order-create/order-create-dispatcher/order-create-dispatcher.component';
import { OrderEditDispatcherComponent } from './order-edit/order-edit-dispatcher/order-edit-dispatcher.component';
import { OrderEditAdminComponent } from './order-edit/order-edit-admin/order-edit-admin.component';
import {MaterialsResolver} from '../../../../_resolvers/materials-list';
import {MaterialService} from '../../../../_services/material.service';

const routes: Routes = [
    {
        'path': '',
        "component": DefaultComponent,
        'children': [
            {
                path: '',
                component: OrdersComponent
            },
            {
                path: 'new',
                component: OrderCreateComponent,
                resolve : {
                    performers : PerformersListResolver,
                    materials : MaterialsResolver
                }
            },
            {
                path: 'edit',
                component: OrderEditComponent,
                resolve : {
                    performers : PerformersListResolver,
                    data : OrderEditResolver,
                    materials : MaterialsResolver
                }
            },

        ]
    }
];
@NgModule({
  imports: [
      CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, ReactiveFormsModule
  ],
    exports : [
        RouterModule, FormsModule, ReactiveFormsModule
    ],
  declarations: [OrdersComponent, OrderCreateComponent, OrderEditComponent, OrdersAdminComponent, OrdersDispatcherComponent, OrderCreateAdminComponent, OrderCreateDispatcherComponent, OrderEditDispatcherComponent, OrderEditAdminComponent],
    providers: [
        OrderEditResolver,
        OrderService,
        PerformerService,
        PerformersListResolver,
        MaterialsResolver,
        MaterialService
    ]
})
export class OrdersModule { }
