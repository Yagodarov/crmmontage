import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import * as moment from 'moment';
import {OrderService } from '../../../../../../_services/order.service';
import {Order} from '../../../../../../_models/order';
import {UserHelper} from '../../../../../../auth/_helpers/user.helper';

declare var $: any;
@Component({
    selector: 'app-orders-dispatcher',
    templateUrl: './orders-dispatcher.component.html',
    styles: [],
    providers: [UserHelper]
})
export class OrdersDispatcherComponent implements OnInit {
    url = 'orders';
    order = new Order();
    title = 'Заказы';
    itemTitle = 'Заказ';
    datatable : any;
    modal_data = {
        material_price : '',
        payment_price : '',
        total : '',
    };
    search : any = {
        execution_date : '',
        number : '',
        customer : '',
        address : ''
    };
    constructor(private router: Router, private _titleService: Title, private _service : OrderService, private user:UserHelper) {
        window['angular'] = this;
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log(this.search);
    }

    ngOnInit() {
        this.datatable = $(".orders").mDatatable({
            data: {
                type: "remote",
                source: {

                    read: {
                        url: 'api/'+this.url,
                        method: 'GET',
                        // custom headers
                        headers: {
                            'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser'))['token']
                        },
                        map: (t) => {
                            var e = t;
                            console.log(this.search);
                            return void 0 !== t.models && (e = t.models), e
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0,
            },
            rows : {
                beforeTemplate: function (row, data, index) {
                    switch (data.status) {
                        case 1 :
                            row.addClass('bg-1');
                            break;
                        case 2 :
                            row.addClass('bg-2');
                            break;
                        case 3 :
                            row.addClass('bg-3');
                            break;
                        case 4 :
                            row.addClass('bg-4');
                            break;
                        case 5 :
                            row.addClass('bg-5');
                            break;
                    }

                }
            },
            layout: {
                scroll: !1,
                class: 'table-bordered',
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            toolbar: {items: {pagination: {pageSizeSelect: [10, 20, 30, 50, 100]}}},
            // search: {input: $("#number")},
            // search: this.search,
            columns: [
                {
                    field: "id",
                    title: "#",
                    sortable: !1,
                    width: 40,
                    selector: !1,
                    textAlign: "center",
                    template : (t) => {
                        var html = '<a class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon-only m-btn--pill" onclick="angular.edit('+t.id+')" title="Edit">'+t.id+'</a>';
                        return html;
                    }
                },
                {
                    field: 'execution_date',
                    title: 'Дата и время подачи',
                    sortable: 'asc|desc',
                    overflow: 'visible',
                    width: 80,
                    template: function (t, a, e) {
                        console.log(t.execution_date);
                        return moment(moment.utc(t.execution_date).toDate()).local().format('DD.MM.YYYY HH:mm');
                    }
                },
                {
                    field: 'customer',
                    title: 'Организация/ФИО заказчика',
                    sortable: 'asc|desc',
                    overflow: 'visible',
                    width: 140,
                },
                {
                    field: 'address',
                    title: 'Адрес обьекта',
                    sortable: 'asc|desc',
                    overflow: 'visible',
                    width: 140,
                },
                {
                    field: 'phone_number',
                    title: 'Номер телефона',
                    sortable: 'asc|desc',
                    overflow: 'visible',
                    width: 140,
                },
                {
                    field: 'comment',
                    title: 'Комментарий к заказу',
                    sortable: 'asc|desc',
                    overflow: 'visible',
                    width: 140,
                },
                {
                    field: 'payment_type',
                    title: 'Тип оплаты',
                    sortable: 'asc|desc',
                    overflow: 'visible',
                    template : (t) => {
                        let obj = this.order.getPaymentStatusList().find(o => o.id === t.payment_type);
                        if (obj) {
                            return obj.label;
                        } else {
                            return ''
                        }
                    },
                    width: 70,
                },
                {
                    field: 'payments',
                    title: 'Стоимость заказа',
                    sortable: 'asc|desc',
                    overflow: 'visible',
                    width: 140,
                    template : function(t) {
                        let resultHtml = '';
                        let cost = t.cost;
                        let payed = 0;
                        t.payments.forEach((payment) => {
                            payed += payment.payment;
                        });
                        if (payed != 0) {
                            let currentDiv = '<div class=\'alert alert-success no-padding-div text-center\'>Оплачено<br>'+payed+' руб.'+'</div>';
                            resultHtml += currentDiv;
                        }
                        if (cost - payed > 0)
                        {
                            let waitForPayment = cost - payed;
                            let currentDiv = '<div class=\'alert alert-warning no-padding-div text-center\'>Ожидаем оплаты<br>'+waitForPayment+' руб.'+'</div>';
                            resultHtml += currentDiv;
                        }
                        return resultHtml;
                    }
                },
                {
                    field: 'jobs',
                    title: '' +
                    '<div class="row">' +
                    '<div class="col-lg-2 table-cell-align">Вид работ</div>' +
                    '<div class="col-lg-2 table-cell-align">Статус заявки</div>' +
                    '<div class="col-lg-4 table-cell-align">Исполнитель</div>' +
                    '<div class="col-lg-4 table-cell-align">Дата и время <br>обработки</div>' +
                    '</div>',
                    sortable: !1,
                    overflow: 'visible',
                    width: 600,
                    template : function(t) {
                        function getOrderStatusById(id)
                        {
                            var order = new Order();
                            let obj = order.getOrderStatusList().find(o => o.id === id);
                            if (obj)
                                return obj.label;
                            else
                                return "Нет статуса";
                        }
                        function getName(item) {
                            if (item) {
                                return item.surname+' '+item.first_name;
                            }
                            else {
                                return '';
                            }
                        }
                        function getDateFromJob(item) {
                            if (item.date) {
                                var stillUtc = moment.utc(item.date).toDate();
                                var local = moment(stillUtc).local().format('DD.MM.YYYY HH:mm');
                                return local;
                            }
                            else {
                                return '';
                            }
                        }
                        function getPrice(price) {
                            let returnVal = '';
                            price ? returnVal =  price : returnVal =  '';
                            return returnVal;
                        }
                        function getDate(date) {
                            var stillUtc = moment.utc(date).toDate();
                            var local = moment(stillUtc).local().format('DD.MM.YYYY HH:mm');
                            return local;
                        }
                        function getUserName(t){
                            if (t.user && t.user.username) {
                                return t.user.username;
                            } else return '';
                        }
                        function getStatusById(status_id) {
                            var values = [
                                {
                                    id : 1,
                                    label : 'Назначен'
                                },
                                {
                                    id : 2,
                                    label : 'На исполнении'
                                },
                                {
                                    id : 3,
                                    label : 'Выполнен'
                                },
                            ];
                            let obj = values.find(o => o.id === status_id);
                            if (obj)
                                return obj.label;
                            else
                                return '';
                        }

                        return '<div class="datatable-div">' +
                            '<div class="row no-padding">' +
                            '<div class="col-lg-4 table-cell-align">'+getOrderStatusById(t.status)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getUserName(t)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getDate(t.status_updated_at)+'</div>' +
                            '</div>' +
                            '<div class="row no-padding">' +
                            '<div class="col-lg-2 table-cell-align">Замер</div>' +
                            '<div class="col-lg-2 table-cell-align">'+getStatusById(t.jobs[0].status)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getName(t.jobs[0].performer)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getDateFromJob(t.jobs[0])+'</div>' +
                            '</div>'+
                            '<div class="row no-padding">' +
                            '<div class="col-lg-2 table-cell-align">Расчет</div>' +
                            '<div class="col-lg-2 table-cell-align">'+getStatusById(t.jobs[1].status)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getName(t.jobs[1].performer)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getDateFromJob(t.jobs[1])+'</div>' +
                            '</div>'+
                            '<div class="row no-padding">' +
                            '<div class="col-lg-2 table-cell-align">Продажа</div>' +
                            '<div class="col-lg-2 table-cell-align">'+getStatusById(t.jobs[2].status)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getName(t.jobs[2].performer)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getDateFromJob(t.jobs[2])+'</div>' +
                            '</div>'+
                            '<div class="row no-padding">' +
                            '<div class="col-lg-2 table-cell-align">Комплектация</div>' +
                            '<div class="col-lg-2 table-cell-align">'+getStatusById(t.jobs[3].status)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getName(t.jobs[3].performer)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getDateFromJob(t.jobs[3])+'</div>' +
                            '</div>'+
                            '<div class="row no-padding">' +
                            '<div class="col-lg-2 table-cell-align">Доставка</div>' +
                            '<div class="col-lg-2 table-cell-align">'+getStatusById(t.jobs[4].status)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getName(t.jobs[4].performer)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getDateFromJob(t.jobs[4])+'</div>' +
                            '</div>'+
                            '<div class="row no-padding">' +
                            '<div class="col-lg-2 table-cell-align">Монтаж</div>' +
                            '<div class="col-lg-2 table-cell-align">'+getStatusById(t.jobs[5].status)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getName(t.jobs[5].performer)+'</div>' +
                            '<div class="col-lg-4 table-cell-align">'+getDateFromJob(t.jobs[5])+'</div>' +
                            '</div>'+
                            '</div>';
                    }
                },
            ]
        });
        this.datatable.on('m-datatable--on-ajax-fail', ($param) => {
            this.user.logout();
        });
        this.setSearchOptions();
        this.setTitle();
    }

    setSearchOptions()
    {
        $("#id").on("change",() => {
            this.datatable.search($("#id").val(), "id");
        });
        $("#execution_date").on("change",() => {
            this.datatable.search($("#execution_date").val(), "execution_date");
        });
        $("#customer").on("change",() => {
            this.datatable.search($("#customer").val(), "customer");
        });
        $("#address").on("change",() => {
            this.datatable.search($("#address").val(), "address");
        });
        $("#phone_number").on("change",() => {
            this.datatable.search($("#phone_number").val(), "phone_number");
        });
        $("#comment").on("change",() => {
            this.datatable.search($("#comment").val(), "comment");
        });
        $("#status").on("change",() => {
            this.datatable.search($("#status").val(), "status");
        });
    }
    edit(item_id)
    {
        this.router.navigate([this.url+'/edit'], {queryParams: {'id' : item_id}});
    }

    priceModal(payment, cost, total) {
        this.modal_data.payment_price = payment;
        this.modal_data.material_price = cost;
        this.modal_data.total = total;
        $('#m_modal_1_2').modal();
    }

    delete(item_id)
    {
        if (confirm('Вы уверены?'))
        {
            this._service.delete(item_id).subscribe(() => {
                this.datatable.reload();
            }, () => {
                alert('Не удалось удалить');
            })
        }
    }

    setTitle() {
        this._titleService.setTitle( this.title );
    }

}
