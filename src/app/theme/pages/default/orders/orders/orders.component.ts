import {Component, OnInit} from '@angular/core';
import {UserHelper} from '../../../../../auth/_helpers/user.helper';

declare var $: any;
@Component({
  selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: [
        'orders.component.css'
    ],
    providers: [UserHelper]
})
export class OrdersComponent implements OnInit {
    user_role;
    constructor(private user : UserHelper) {

    }

    ngOnInit() {
        this.user_role = this.user.getUserRole();
    }
}
