import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PerformerService as Service} from '../../../../../_services/performer.service';
import { Title} from '@angular/platform-browser';
import {Router} from '@angular/router';

@Component({
    selector: 'app-performer-create',
    templateUrl: './performer-create.component.html',
    styles: []
})
export class PerformerCreateComponent implements OnInit {
    url = 'performers';
    title = 'Добавить исполнителя';
    button_label = 'Добавить исполнителя';
    errors = [];
    form: FormGroup;

    constructor(private _fb: FormBuilder, private _location: Location,
                private _service: Service,
                private _titleService: Title,
                private router: Router
    ) {
        this.initForm();
    }

    initForm() {
        this.form = this._fb.group({
            first_name: '',
            surname: '',
            patronymic: '',
            phone: ''
        });
    }

    submitForm() {
        this._service.create(this.form.value).subscribe((data) => {
            this.router.navigate(['/' + this.url]);
        }, (data) => {
            this.errors = data['error'];
            this.scrollToError();
        });
        console.log(this.form);
    }

    scrollToError() {
        var id = '#' + Object.keys(this.errors)[0] + 'Input';
        $('html, body').animate({scrollTop: $(id).position().top - 170}, 400);
    }

    cancelCreate() {
        this._location.back();
    }

    ngOnInit() {
        this.setTitle();
    }

    getInputClass(field) {
        let classList = {
            'has-danger': this.errors[field] != undefined ? true : false
        };
        return classList;
    }

    setTitle() {
        this._titleService.setTitle( this.title );
    }
}
