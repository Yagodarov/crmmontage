import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PerformerService} from '../../../../../_services/performer.service';
import { Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-performer-edit',
    templateUrl: './performer-edit.component.html',
  styles: []
})
export class PerformerEditComponent implements OnInit {
    url = 'performers';
    title = 'Редактировать исполнителя';
    button_label = 'Редактировать исполнителя';
    errors = [];
    form: FormGroup;

    constructor(private _fb: FormBuilder, private _location: Location,
                private _service: PerformerService,
                private _titleService: Title,
                private route: ActivatedRoute,
                private router: Router
    ) {
        this.initForm();
    }

    initForm() {
        this.form = this._fb.group({
            id: '',
            first_name: '',
            surname: '',
            patronymic: '',
            phone: ''
        });
    }

    submitForm() {
        this._service.update(this.form.value).subscribe((data) => {
            this.router.navigate(['/' + this.url]);
        }, (data) => {
            this.errors = data['error'];
            this.scrollToError();
        });
        console.log(this.form);
    }

    scrollToError() {
        var id = '#' + Object.keys(this.errors)[0] + 'Input';
        $('html, body').animate({scrollTop: $(id).position().top - 170}, 400);
    }

    cancelCreate() {
        this._location.back();
    }

    ngOnInit() {
        this.initFormValues();
        this.setTitle();
    }
    initFormValues() {
        this.route.data
            .subscribe((data) => {
                console.log(data);
                this.form.patchValue(data['data'], {
                    emitEvent : false,
                    onlySelf : true
                });
            });
    }
    getInputClass(field) {
        let classList = {
            'has-danger': this.errors[field] != undefined ? true : false
        };
        return classList;
    }

    setTitle() {
        this._titleService.setTitle( this.title );
    }
}
