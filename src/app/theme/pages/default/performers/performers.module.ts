import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule}   from '@angular/forms';
import {LayoutModule } from '../../../layouts/layout.module';
import {DefaultComponent} from '../default.component';
import {PerformersComponent} from './performers/performers.component';
import {PerformerCreateComponent} from './performer-create/performer-create.component';
import {PerformerEditComponent} from './performer-edit/performer-edit.component';
import {PerformerEditResolver} from '../../../../_resolvers/performer-edit';
import {PerformerService} from '../../../../_services/performer.service';

const routes: Routes = [
    {
        'path': '',
        "component": DefaultComponent,
        'children': [
            {
                path: '',
                component: PerformersComponent
            },
            {
                path: 'new',
                component: PerformerCreateComponent
            },
            {
                path: 'edit',
                component: PerformerEditComponent,
                resolve : {
                    data : PerformerEditResolver
                }
            },

        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, ReactiveFormsModule
    ], exports: [
        RouterModule, FormsModule, ReactiveFormsModule
    ], declarations: [
        PerformersComponent,
        PerformerCreateComponent,
        PerformerEditComponent,
    ], providers: [
        PerformerService,
        PerformerEditResolver,
    ]
})
export class PerformersModule {


}