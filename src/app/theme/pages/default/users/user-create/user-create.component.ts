import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {UserService} from '../../../../../_services/user.service';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
@Component({
    selector: 'app-user-create',
    templateUrl: './user-create.component.html',
    styles: []
})
export class UserCreateComponent implements OnInit {
    title = "Добавить пользователя";
    button_label = "Добавить пользователя";
    errors = [];
    form : FormGroup;
    constructor(private _fb: FormBuilder, private _location: Location,
                private _service: UserService, private _titleService: Title, private router: Router
    ) {
        this.initForm();
    }

    initForm()
    {
        this.form = this._fb.group({
            // username : new FormControl(''),
            username : '',
            password : '',

            first_name : '',
            surname : '',
            patronymic : '',

            email : '',
            role : 'dispatcher',
        });
    }

    submitForm()
    {
        this._service.create(this.form.value).subscribe((data) => {
            this.router.navigate(['/users']);
        }, (data) => {
            this.errors = data['error'];
            this.scrollToError();
        });
        console.log(this.form);
    }

    scrollToError()
    {
        var id = '#'+Object.keys(this.errors)[0] + 'Input';
        // $('body').scrollTo(id)
        $('html, body').animate({scrollTop: $(id).position().top- 170}, 400);
    }
    cancelCreate()
    {
        this._location.back();
    }

    ngOnInit() {
        this.setTitle();
    }

    getInputClass(field)
    {
        let classList = {
            'has-danger' : this.errors[field] != undefined ? true : false
        };
        return classList;
    }

    setTitle() {
        this._titleService.setTitle( this.title );
    }
}
