import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {UserService} from '../../../../../_services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styles: []
})
export class UserEditComponent implements OnInit {
    title = 'Редактировать пользователя';
    button_label = 'Редактировать пользователя';
    errors = [];
    form: FormGroup;

    constructor(private _fb: FormBuilder, private _location: Location, private _titleService: Title,
                private _service: UserService, private route: ActivatedRoute, private router: Router
    ) {
        this.initForm();
    }

    initForm() {
        this.form = this._fb.group({
            id : '',
            username: '',
            password: new FormControl(null),

            first_name: '',
            surname: '',
            patronymic: '',

            blocked: 0,

            email: '',
            role: '',
        });
    }

    submitForm() {
        this._service.update(this.form.value).subscribe((data) => {
            this.router.navigate(['/users']);
        }, (data) => {
            this.errors = data['error'];
            console.log(Object.keys(this.errors)[0]);
            this.scrollToError();
        });
        console.log(this.form);
    }

    scrollToError()
    {
        var id = '#'+Object.keys(this.errors)[0] + 'Input';
        // $('body').scrollTo(id);
        $('html, body').animate({scrollTop: $(id).position().top- 170}, 400);
    }

    cancelCreate() {
        this._location.back();
    }

    ngOnInit() {
        this.initFormValues();
        this.setTitle();
    }

    initFormValues() {
        this.route.data
            .subscribe((data) => {
                console.log(data);
                this.form.patchValue(data['data'], {
                    emitEvent : false,
                    onlySelf : true
                });
            });
    }

    getInputClass(field) {
        let classList = {
            'has-danger': this.errors[field] != undefined ? true : false
        };
        return classList;
    }

    setTitle() {
        this._titleService.setTitle( this.title );
    }
}
