import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule}   from '@angular/forms';
import {LayoutModule } from '../../../layouts/layout.module';
import {UsersComponent} from './users/users.component';
import {DefaultComponent} from '../default.component';
import {UserCreateComponent} from './user-create/user-create.component';
import {UserEditComponent} from './user-edit/user-edit.component';
import {UserService} from '../../../../_services/user.service';
import {UserEditResolver} from '../../../../_resolvers/user-edit';

const routes: Routes = [
    {
        'path': '',
        "component": DefaultComponent,
        'children': [
            {
                path: '',
                component: UsersComponent
            },
            {
                path: 'new',
                component: UserCreateComponent
            },
            {
                path: 'edit',
                component: UserEditComponent,
                resolve : {
                    data : UserEditResolver
                }
            },

        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, ReactiveFormsModule
    ], exports: [
        RouterModule, FormsModule, ReactiveFormsModule
    ], declarations: [
        UsersComponent,
        UserCreateComponent,
        UserEditComponent
    ], providers: [
        UserService,
        UserEditResolver
    ]
})
export class UsersModule {


}