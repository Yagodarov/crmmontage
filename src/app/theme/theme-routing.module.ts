import {NgModule} from '@angular/core';
import { ThemeComponent } from './theme.component';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from '../auth/_guards';

const routes: Routes = [
    {
        "path": "",
        "component": ThemeComponent,
        "canActivate": [AuthGuard],
        "children": [
            {
                "path" : "users",
                "loadChildren": ".\/pages\/default\/users\/users.module#UsersModule"
            },
            {
                "path" : "performers",
                "loadChildren": ".\/pages\/default\/performers\/performers.module#PerformersModule"
            },
            {
                "path" : "orders",
                "loadChildren": ".\/pages\/default\/orders\/orders.module#OrdersModule"
            },
            {
                "path" : "materials",
                "loadChildren": ".\/pages\/default\/materials\/materials.module#MaterialsModule"
            },
            {
                "path": "404",
                "loadChildren": ".\/pages\/default\/not-found\/not-found.module#NotFoundModule"
            },
            {
                "path": "",
                "redirectTo": "orders",
                "pathMatch": "full"
            }
        ]
    },
    {
        "path": "snippets\/pages\/user\/login-1",
        "loadChildren": ".\/pages\/self-layout-blank\/snippets\/pages\/user\/user-login-1\/user-login-1.module#UserLogin1Module"
    },
    {
        "path": "**",
        "redirectTo": "404",
        "pathMatch": "full"
    }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ThemeRoutingModule {}